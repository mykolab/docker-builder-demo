stages:
  - build
  - test
  - deploy

variables:
  ENV_NAME: staging
  PROJECT_DIR: /var/www/backend
  PROJECT_USER: web

##############
# Conditions #
##############
.if-staging-merge-request: &if-staging-merge-request
  if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "dev"'

.if-production-merge-request: &if-production-merge-request
  if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'

.if-staging-branch: &if-staging-branch
  if: '$CI_COMMIT_BRANCH == "dev"'

.if-production-branch: &if-production-branch
  if: '$CI_COMMIT_BRANCH == "main"'

.rules:docker:
  rules:
    - <<: *if-staging-merge-request
      changes:
        - docker/**/*

.rules:default:
  rules:
    - <<: *if-staging-merge-request
    - <<: *if-production-merge-request
    - <<: *if-staging-branch
    - <<: *if-production-branch

workflow:
  rules:
    - <<: *if-production-branch
      variables: # Override ENV_NAME defined
        ENV_NAME: "production"
    - when: always

##############
# Jobs #
##############
Build docker:
  stage: build
  extends:
    - .rules:docker
  image: tmaier/docker-compose:latest
  services:
    - docker:dind
  before_script:
    - docker info
    - docker-compose --version
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - cd docker
    - cp .env.local .env
    - docker-compose build --no-cache
    - docker-compose push

Run PHPUnit tests:
  stage: test
  extends:
    - .rules:default
  image:
    name: registry.gitlab.com/mykolab/docker-builder-demo/workspace:local
    entrypoint: ['bash', '-c', 'exec su tallium -c bash']
  variables:
    POSTGRES_DB: test_db
    POSTGRES_USER: test_user
    POSTGRES_PASSWORD: password
  services:
    - name: registry.gitlab.com/mykolab/docker-builder-demo/postgres:local
      alias: postgres
  before_script:
    - composer install
  script:
    - php artisan test
  needs:
    - job: Build docker
      optional: true

Check code style with CodeSniffer:
  stage: test
  extends:
    - .rules:default
  image:
    name: registry.gitlab.com/mykolab/docker-builder-demo/workspace:local
    entrypoint: ['bash', '-c', 'exec su tallium -c bash']
  before_script:
    - composer install
  script:
    - ./vendor/bin/phpcs --standard=PSR12 app/
  needs:
    - job: Build docker
      optional: true

.base_deployment: &base_deployment
  stage: deploy
  image: php:8.1
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - ssh-add <(echo "$DEPLOYMENT_SSH_PRIVATE_KEY")
    - php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer
    - composer global require laravel/envoy
    - export PATH=~/.composer/vendor/bin:$PATH
  script:
    - echo "Start deploy to staging ..."
    - envoy run deploy --env=staging
  needs:
    - Run PHPUnit tests
    - Check code style with CodeSniffer

Deploy staging:
  <<: *base_deployment
  rules:
    - <<: *if-staging-branch
  environment:
    name: staging
    url: https://api.db-demo.bokoch-dev.site

Deploy production:
  <<: *base_deployment
  rules:
    - <<: *if-production-branch
  environment:
    name: production
    url: https://api.production.db-demo.bokoch-dev.site
