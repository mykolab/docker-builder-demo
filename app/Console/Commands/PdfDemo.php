<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Browsershot\Browsershot;

class PdfDemo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pdf:demo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pdf demo';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Browsershot::url('https://pdf.bokoch-dev.site/pdf/360_report.html')
            ->deviceScaleFactor(4)
            ->save('example.pdf');

        return self::SUCCESS;
    }
}
