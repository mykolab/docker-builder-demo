<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function see_user_emails(): void
    {
        $users = User::factory()->count(3)->create();

        $this->get(route('users.index'))
            ->assertOk()
            ->assertSee($users->pluck('email')->toArray());
    }
}
