#!/bin/sh
set -e

function redis_config_envsubst() {
    local redis_config_dir="/usr/local/etc/redis"
    local redis_config_template="$redis_config_dir/redis.conf.template"
    local output_path="$redis_config_dir/redis.conf"
    local defined_envs=$(printf '${%s} ' $(env | cut -d= -f1))

    echo "Passing env variables to redis.conf.template and generate redis.conf file."
    envsubst "$defined_envs" < "$redis_config_template" > "$output_path"
}

redis_config_envsubst

# first arg is `-f` or `--some-option`
# or first arg is `something.conf`
if [ "${1#-}" != "$1" ] || [ "${1%.conf}" != "$1" ]; then
	set -- redis-server "$@"
fi

# allow the container to be started with `--user`
#if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
#	find . \! -user redis -exec chown redis '{}' +
#	exec gosu redis "$0" "$@"
#fi

exec "$@"
