FROM phusion/baseimage:focal-1.1.0

LABEL maintainer="Tallium <dev@tallium.com>"

ENV DEBIAN_FRONTEND=noninteractive
RUN locale-gen en_US.UTF-8

ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LC_CTYPE=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV TERM xterm

ARG PHP_VERSION=8.1

ARG NODE_VERSION=16
ENV NODE_VERSION ${NODE_VERSION}

# Set Timezone
ARG TZ=UTC
ENV TZ ${TZ}

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Configure non-root user.
ARG USER_ID=1000
ARG GROUP_ID=1000

RUN groupadd --force -g ${GROUP_ID} tallium
RUN useradd -ms /bin/bash --no-user-group -g ${GROUP_ID} -u ${USER_ID} tallium

RUN set -eux \
    # Install php and it's extestions
    && apt-get install -y software-properties-common \
    && add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-dev \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-sqlite \
        php${PHP_VERSION}-sqlite3 \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-soap \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-redis \
        php${PHP_VERSION}-dev \
        pkg-config \
        libcurl4-openssl-dev \
        libedit-dev \
        libssl-dev \
        libxml2-dev \
        xz-utils \
        libsqlite3-dev \
        rsync \
        sqlite3 \
        git \
        curl \
        nano \
        tree

RUN update-alternatives --set php /usr/bin/php${PHP_VERSION}

# Install composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer
COPY ./auth.json /home/tallium/.composer/auth.json
RUN chown -R tallium:tallium /home/tallium/.composer

ARG INSTALL_PGSQL=false
RUN if [ ${INSTALL_PGSQL} = true ]; then \
    set -ex; \
    apt-get install -yqq \
      postgresql-client \
      php${PHP_VERSION}-pgsql \
;fi

ARG INSTALL_MYSQL=false
RUN if [ ${INSTALL_MYSQL} = true ]; then \
    set -ex; \
    apt-get install -yqq  \
      mysql-client \
      php${PHP_VERSION}-mysql \
;fi

ARG INSTALL_INTL=false
RUN if [ ${INSTALL_INTL} = true ]; then \
    set -eux; \
    apt-get install -yqq php${PHP_VERSION}-intl \
;fi

ARG INSTALL_WKHTMLTOPDF=false
RUN if [ ${INSTALL_WKHTMLTOPDF} = true ]; then \
    apt-get install -yqq \
      libxrender1 \
      libfontconfig1 \
      libx11-dev \
      libjpeg62 \
      libxtst6 \
      fontconfig \
      xfonts-base \
      xfonts-75dpi \
      wkhtmltopdf \
;fi

#TODO: Add xdebug

ARG INSTALL_IMAGEMAGICK=false
RUN if [ ${INSTALL_IMAGEMAGICK} = true ]; then \
    apt-get install -yqq imagemagick php${PHP_VERSION}-imagick \
;fi

# Aliases
USER root

COPY ./aliases.sh /root/aliases.sh
COPY ./aliases.sh /home/tallium/aliases.sh

RUN sed -i 's/\r//' /root/aliases.sh && \
    sed -i 's/\r//' /home/tallium/aliases.sh && \
    chown tallium:tallium /home/tallium/aliases.sh && \
    echo "" >> ~/.bashrc && \
    echo "# Load Custom Aliases" >> ~/.bashrc && \
    echo "source ~/aliases.sh" >> ~/.bashrc && \
	  echo "" >> ~/.bashrc

USER tallium

RUN echo "" >> ~/.bashrc && \
    echo "# Load Custom Aliases" >> ~/.bashrc && \
    echo "source ~/aliases.sh" >> ~/.bashrc && \
    echo "" >> ~/.bashrc

USER root

# Cleanup
RUN apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /var/www
