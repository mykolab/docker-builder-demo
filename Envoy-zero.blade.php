@setup
    $envToBranch = [
        'staging' => 'dev',
        'production' => 'main',
    ];

    if (! isset($env) || ! in_array($env, array_keys($envToBranch))) {
        throw new Exception(
            sprintf('--env must be specified. Available environments: %s', implode(',', array_keys($envToBranch)))
        );
    }

    $repository = 'git@gitlab.com:mykolab/docker-builder-demo.git';

    $branch = $envToBranch[$env];

    $appBaseDir = '/var/www/backend-envoy';
    $currentReleaseDir = $appBaseDir.'/current';
    $releasesDir = $appBaseDir.'/releases';
    $storageDir = $appBaseDir.'/storage';

    $newReleaseDir = $releasesDir.'/'.date('YmdHis');
@endsetup

@servers(['staging' => ['web@137.184.144.161'], 'production' => ['web@137.184.144.161']])

@story('deploy', ['on' => $env])
    git
    composer
    init_env
    update_links
    migrate
    optimize
    live
@endstory

@task('git')
    git clone -b {{ $branch }} "{{ $repository }}" {{ $newReleaseDir }}
@endtask

@task('composer')
    cd {{ $newReleaseDir }}
    composer install --no-interaction --quiet --no-dev --prefer-dist --optimize-autoloader
@endtask

@task('init_env')
    cd {{ $newReleaseDir }}
    cp .env.staging .env
@endtask

@task('update_links')
    cd {{ $newReleaseDir }}
    rm -rf {{ $newReleaseDir }}/storage
    ln -nfs {{ $storage }} {{ $newReleaseDir }}/storage
@endtask

@task('migrate')
    {{ $newReleaseDir }}/artisan migrate --force
@endtask

@task('optimize')
    {{ $newReleaseDir }}/artisan optimize
@endtask

@task('live')
    cd {{ $newReleaseDir }}
    ln -nfs {{ $newReleaseDir }} {{ $currentReleaseDir }}
@endtask

@finished
    echo "Envoy deployment completed.\r\n";
@endfinished
