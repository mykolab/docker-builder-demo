#!/bin/bash

set -e

if ! [ -x "$(command -v docker-compose)" ]; then
    echo 'Docker-compose is not installed.' >&2
    exit 1
fi

env_name="$1"
git_target_branch="$2"

func_inc=0
info() {
  func_inc=$((func_inc+1));
  local color='\033[0;32m'
  local nc='\033[0m';
  echo -e "\n${color}[Project] $func_inc. $1${nc}";
}

error() {
  local color='\033[0;31m'
  local nc='\033[0m';
  echo -e "\n${color}[Project] Error: $1${nc}";
}

if [ -z "$env_name" ]; then
    error 'No environment name was not passed as argument.' >&2
    exit 1
fi

if [ -z "$git_target_branch" ]; then
    error 'No git target branch was not passed as argument.' >&2
    exit 1
fi

info "Checkout to ${git_target_branch}"
git checkout .
git checkout ${git_target_branch}
info "Pulling latest changes"
git pull

if ! [ -f "./.env.${env_name}" ]; then
    error ".env.${env_name} file is not found." >&2
    exit 1
fi

info "Copying .env.${env_name} to .env"
cp .env.${env_name} .env

info "Deployment was finished."
