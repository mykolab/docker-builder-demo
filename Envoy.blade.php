@setup
    $envToBranch = [
        'staging' => 'dev',
        'production' => 'main',
    ];

    if (! isset($env) || ! in_array($env, array_keys($envToBranch))) {
        throw new Exception(
            sprintf('--env must be specified. Available environments: %s', implode(',', array_keys($envToBranch)))
        );
    }

    $branch = $envToBranch[$env];
    $appDir = "/var/www/backend";
@endsetup

@servers(['staging' => ['web@137.184.144.161'], 'production' => ['web@137.184.144.161']])

@story('deploy', ['on' => $env])
    git
    composer
    migrate
    optimize
@endstory

@task('git')
    cd {{ $appDir }}
    git checkout .
    git checkout {{ $branch }}
    git pull origin {{ $branch }}
@endtask

@task('composer')
    cd {{ $appDir }}
    composer install --no-interaction --quiet --no-dev --prefer-dist --optimize-autoloader
@endtask

@task('migrate')
    {{ $appDir }}/artisan migrate --force
@endtask

@task('optimize')
    {{ $appDir }}/artisan optimize
@endtask

@finished
    echo "Envoy deployment completed.\r\n";
@endfinished
